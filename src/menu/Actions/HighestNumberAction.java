package menu.Actions;

import context.Context;
import menu.Action;

public class HighestNumberAction implements Action {

    @Override
    public void execute() {

        int numberMax = 0;
        int current;
        int number = Context.getInstance().randomNumber.nextInt(899 + 1);
        number += 100;
        System.out.println("Ваше число: " + number);

        while(number > 0 && numberMax != 9){
            if ((current = number % 10) > numberMax)
                numberMax = current;
            number /= 10;
        }
        System.out.println("Самое большое число: " + numberMax);
    }
}
