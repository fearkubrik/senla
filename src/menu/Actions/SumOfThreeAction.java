package menu.Actions;

import context.Context;
import menu.Action;

public class SumOfThreeAction implements Action {

    @Override
    public void execute() {
        int counter = 0;
        int number = Context.getInstance().randomNumber.nextInt(899 + 1);
        number += 100;

        System.out.println("Число: " + number);

        counter += number % 10;
        number = number / 10;
        counter += number % 10;
        number = number / 10;
        counter += number;

        System.out.println("Сумма его чисел: " + counter);
    }

}
