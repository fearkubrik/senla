package menu.Actions;

import menu.Action;

public class ExitAction implements Action {

    @Override
    public void execute() {
        System.out.println("ВЫХОД...");
        System.exit(0);
    }

}
