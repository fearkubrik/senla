package menu.Actions;

import context.Context;
import menu.Action;

public class SumOfFirstAction implements Action {

    @Override
    public void execute() {

        int summ = 0;

        for (int i = 0; i < 3; i++){
            int number = Context.getInstance().randomNumber.nextInt(899 + 1);
            number += 100;
            System.out.print(number + " ");
            number /= 100;
            summ += number;

        }
        System.out.println("Сумма первых трёх чисел: " + summ);
    }
}
