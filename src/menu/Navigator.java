package menu;

import context.Context;

public class Navigator {


    private Menu currentMenu;

    public void setCurrentMenu(Menu currentMenu) { this.currentMenu = currentMenu;}

    public Menu getCurrentMenu() {
        return currentMenu;
    }

    private static Navigator instance;

    private Navigator() {}

    public static Navigator getInstance() {
        if (instance == null) {
            instance = new Navigator();
        }
        return instance;
    }

    public void printMenu() {
        for (MenuItem menuItem : instance.getCurrentMenu().getMenuItems()) {
            System.out.println(menuItem.getKey() + menuItem.getTitle());
        }
    }


    public void navigate() {
        String key = userChoise();
        MenuItem menuChoise = null;

        for (MenuItem menuItem : instance.getCurrentMenu().getMenuItems()){
            if (key.equals(menuItem.getKey())){
                menuChoise = menuItem;
                break;
            }
        }
        if (menuChoise != null) {
            menuChoise.executeAction();
        }
        else System.out.println("Ошибочка вышла...");
    }



    private String userChoise() {
        System.out.print("Введите: ");
        String in = Context.getInstance().key.next();
        return in;
    }
}
