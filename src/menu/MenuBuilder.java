package menu;

import menu.Actions.*;

public class MenuBuilder {

    public Menu mainMenu;

    public Menu getMainMenu() {
        return mainMenu;
    }

    private static MenuBuilder instance;

    private MenuBuilder() {
        buildMenu();
    }

    public static MenuBuilder getInstance() {
        if (instance == null) {
            instance = new MenuBuilder();
        }
        return instance;
    }


    public void buildMenu() {
        mainMenu = new Menu("Main");
        mainMenuInit();

        Navigator.getInstance().setCurrentMenu(mainMenu);
    }

    public void mainMenuInit() {
        mainMenu.getMenuItems().add(new MenuItem("1", ".Трёхзначное число и его самая большая цифра", new HighestNumberAction()));
        mainMenu.getMenuItems().add(new MenuItem("2", ".Три трёхначных числа и сумма их 3 первых чисел", new SumOfFirstAction()));
        mainMenu.getMenuItems().add(new MenuItem("3", ".Три трёхзначных числа и разница между ними", new DifferenceAction()));
        mainMenu.getMenuItems().add(new MenuItem("4", ".Трёхзначное число и сумма его чисел", new SumOfThreeAction()));
        mainMenu.getMenuItems().add(new MenuItem("5", ".ВЫХОД", new ExitAction()));
    }

}
