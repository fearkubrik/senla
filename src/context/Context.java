package context;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Context {

    private static volatile Context instance;

    public static Random randomNumber = new Random();

    public static Scanner key = new Scanner(System.in);

    public static Context getInstance() {

        if (instance == null)   {
            synchronized (Context.class)    {
                if (instance == null)
                    instance = new Context();
            }
        }
        return instance;
    }
}
