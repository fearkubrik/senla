package context;

import menu.MenuController;


public class MainInitializer {

    public static void init() {
        initContext();
        initMenu();
    }

    private static void initContext() {
        Context.getInstance();
        Context.getInstance();

    }

    private static void initMenu() {
        MenuController.getInstance().run();
    }
}
